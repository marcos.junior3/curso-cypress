Cypress.Commands.add("preencheCamposObrigatorios", (data) => {
    cy.get("#first-name").type(data.firstName)
    cy.get("#last-name").type(data.lastname)
    cy.get("#email").type(data.email)
    cy.get("#agree").check()
})
