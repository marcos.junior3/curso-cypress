describe("fluxo e2e", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))

    it("fluxo e2e", () => {
        const firstName = "Marcos"
        const lastname = "Pereira"
        const fullName = `${firstName} ${lastname}`

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastname)
        cy.get("#email").type("marcos@teste.com.br")
        cy.get("#ticket-quantity").select("3")
        cy.get("#vip").check()
        cy.get("#friend").check()
        cy.get("#requests").type("Cerveja")

        cy.get(".agreement p")
            .should("contain", `I, ${fullName}, wish to buy 3 VIP tickets.`)

        cy.get("#agree").check()
        cy.get("#signature").type(fullName)

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("button[type='reset']").click()

        cy.get("@submitButton").should("be.disabled")
    })
})