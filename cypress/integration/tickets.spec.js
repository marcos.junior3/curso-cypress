describe("Ticket", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))

    it("preenchimento de todos os campos", () => {
        const firstName = "Marcos"
        const lastname = "Pereira"

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastname)
        cy.get("#email").type("marcos@teste.com.br")
        cy.get("#requests").type("Normal")
        cy.get("#signature").type(`${firstName} ${lastname}`)
    })

    it("selecionando quantidade combobox", () => {
        cy.get("#ticket-quantity").select("2")
    })

    it("interagindo com radiobuttons", () => {
        cy.get("#vip").check()
    })

    it("interagindo com checkboxes", () => {
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#social-media").check()
        cy.get("#publication").uncheck()
    })

    it("Tem um header chamado 'TICKETBOX'", () => {
        cy.get("header h1").should("contain", "TICKETBOX")
    })

    it("alerta de email invalido", () => {
        cy.get("#email")
            .as("email")
            .type("testexpto")

        cy.get("#email.invalid").should("exist")

        cy.get("@email")
            .clear()
            .type("marcos.junior@teste.com.br")

        cy.get("#email.invalid").should("not.exist")
    })

    it("preenche campos obrigatorios usando comandos de suporte", () => {
        const customer = {
            firstName: "João",
            lastname: "da Silva",
            email: "joaosilva@teste.com"
        }

        cy.preencheCamposObrigatorios(customer)

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("button[type='reset']").click()

        cy.get("@submitButton").should("be.disabled")
    })
});